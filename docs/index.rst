.. complexity documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Contents:
=========

.. toctree::
   :maxdepth: 2

   installation
   usage
   contributing
   authors
   history

Feedback
========

If you have any suggestions or questions about **JSON Knife** feel free to email me
at loxosceles@gmx.de.

If you encounter any errors or problems with **JSON Knife**, please let me know!
Open an Issue at the GitLab http://gitlab.com/loxosceles/json_knife main repository.
