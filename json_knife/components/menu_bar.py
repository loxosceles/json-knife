#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Imports
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from pathlib import Path


class MenuBar(ttk.Frame):
    """
    Define namespace, styling and method calls of the menubar.

    The menubar hosts the usual file, edit, about, and other menus with their
    corresponding sub-menus. All sub-menu entries trigger callbacks which can bundle
    information and call a method of the main applications (if necessary).
    Methods inside the menubar are not designed to execute much of the application
    logic, but rather passing events and received user interaction down to the main
    application. The main application takes care of recollecting information from
    other GUI elements and/or the data object.

    """

    def __init__(self, parent, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        self.default_location = Path(Path.home() / 'test/json_knife_data')

        # Create menu bar
        self.menubar = tk.Menu(self)
        self.parent.parent.config(menu=self.menubar)

        # Create file menu
        self.file_menu = tk.Menu(self.menubar, tearoff=0,
                                 postcommand=self.set_file_submenus)
        self.menubar.add_cascade(label='File', menu=self.file_menu)
        # Open
        self.file_menu.add_command(label='Open', accelerator='Ctrl + O',
                compound=tk.LEFT, command=self.open)
        # Save
        self.file_menu.add_command(label='Save', accelerator='Ctrl + S',
                compound=tk.LEFT, state=tk.DISABLED, command=self.save)
        # Save as
        self.file_menu.add_command(label='Save as..', accelerator='Shift + Ctrl + S',
                compound=tk.LEFT, state=tk.DISABLED, command=self.save_as)
        # Quit
        self.file_menu.add_command(label='Quit', accelerator='Ctrl + Q',
                compound=tk.LEFT, command=self.parent.quit)

        # Create edit menu
        self.edit_menu = tk.Menu(self.menubar, tearoff=0,
                                 postcommand=self.set_edit_submenus)
        self.menubar.add_cascade(label='Edit', menu=self.edit_menu)
        # Add object
        self.edit_menu.add_command(label='Add Object', accelerator='Shift + Ctrl + O',
                compound=tk.LEFT, command=self.add_object)

        # Delete object
        self.edit_menu.add_command(label='Delete Object', accelerator='Shift + Ctrl + D',
                compound=tk.LEFT, state=tk.DISABLED, command=self.delete_object)

        # Create about menu
        self.about_menu = tk.Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label='About', menu=self.about_menu)

    def set_file_submenus(self):
        """Enable/disable file menu entries depending on data object state."""

        if self.parent.data_object.is_live() and not self.parent.data_object.is_empty():
            self.file_menu.entryconfig('Save', state=tk.NORMAL)
            self.file_menu.entryconfig('Save as..', state=tk.NORMAL)
        elif not self.parent.data_object.is_empty():
            self.file_menu.entryconfig('Save', state=tk.DISABLED)
            self.file_menu.entryconfig('Save as..', state=tk.NORMAL)
        else:
            self.file_menu.entryconfig('Save', state=tk.DISABLED)
            self.file_menu.entryconfig('Save as..', state=tk.DISABLED)

    def set_edit_submenus(self, event=None):
        """Enable/disable edit menu entries depending on data object state."""

        if not self.parent.data_object.is_empty():
            self.edit_menu.entryconfig('Delete Object', state=tk.NORMAL)
        else:
            self.edit_menu.entryconfig('Delete Object', state=tk.DISABLED)

    def open(self, event=None):
        """Open callback in menubar

        :event:   Callback event
        :returns: None
        """

        file_name = filedialog.askopenfilename(initialdir=self.default_location,
                                               defaultextension=".json",
                                               filetypes=[("Configuration files",
                                                           "*.json")])
        if file_name:
            self.parent.init_data_object(file_name)

    def save(self, event=None):
        """Save callback in menubar

        :event:   Callback event
        :returns: None
        """

        if self.parent.data_object.is_empty():
            # This will appear inside the status bar later
            return

        if not self.parent.data_object.is_live():
            self.save_as()
        else:
            self.parent.write_to_file()
            self.parent.clear_do_tags()
            self.parent.refresh_monitor()
        return "break"

    def save_as(self, event=None):
        """Save as callback in menubar

        :event:    Callback event
        :returns: None
        """

        if self.parent.data_object.is_empty():
            # This will appear inside the status bar later
            return

        file_name = filedialog.asksaveasfilename(defaultextension=".json",
                filetypes=[("Configuration Files", "*.json")])

        if file_name:
            self.parent.set_do_name(file_name)
            self.parent.write_to_file()
            self.parent.set_title()
            self.parent.clear_do_tags()
            self.parent.refresh_monitor()
            return "break"

    def add_object(self, event=None):
        """Trigger action in MainApplication on selection "add object."""

        self.parent.create_dialog()

    def delete_object(self, event=None):

        """Trigger action in MainApplication on selection "delete object."""
        self.parent.delete_dialog()
