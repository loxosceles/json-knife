#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Imports
import tkinter as tk
from tkinter import ttk
from json_knife.utils.vertical_scroll_frame import VerticalScrollFrame
from json_knife.utils.validating_entry import (ArrayEntry,
                                               IntegerEntry,
                                               FloatEntry,
                                               StringEntry)
from json_knife.utils.helpers import flatten_integer_path
#  Constants
ARROW_SYM = ' \u2799 '

class Editor(ttk.Frame):
    """ Define namespace, styling and method calls of the menubar.

    The editor section is where labels and entry widgets, corresponding to the
    JSON key/value pairs, are dynamically created. Initially, the loaded JSON file
    defines any of the entry widgets and their data type. Current values will
    appear inside the boxes and are editable. When a value is changed and the entry
    widget looses focus (by tabbing to the next entry), the modified value is updated
    internally and visualized inside the monitor window. Any changed values differening
    from the ones in the original file (since last save) will be marked in red. Once
    these new values are saved to file, they represent the new state of the file and
    the markers will be resetted.

    Labels will show nested objects by prepending a ' \u2799 ' symbol, so pathes of
    a deeper nested objects are shown as a sequence of names and arrows.
    """

    def __init__(self, parent, screen_height, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.screen_height = screen_height
        self.frame = VerticalScrollFrame(self,
                                         (self.screen_height - (
                                             self.screen_height * 0.3)))
        self.parent = parent

        # style variables
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        self.label_style = ttk.Style()
        self.label_style.configure("label_style.TLabel",
                          justify=tk.LEFT,
                          padding=[0, 10, 0, 5],
                          font=('Arial', 10, 'bold'))

        self.combo_style = ttk.Style()
        self.combo_style.map("combo_style.TCombobox",
                             fieldbackground=[('!disabled', 'white')])

    def _accommodate_rows(self, column, row):
        """ Calculate linebreak for entry boxes."""

        if column < 1:
            column += 1
            return column, row
        else:
            row += 2
            return 0, row

    def _select_entry_by_type(self, value_type):
        return {int: IntegerEntry, float: FloatEntry,
                str: StringEntry}[value_type]

    def _create_list_entry(self, parent, value):
        value = str(value).strip('[]').replace(',', '')
        return ArrayEntry(parent, value=value)

    def _create_combobox_entry(self, parent, value):
        entry = ttk.Combobox(parent, state="readonly",
                             style="combo_style.TCombobox")
        entry["values"] = ["true", "false"]
        bool_to_str = {True: 0, False: 1}
        entry.current(bool_to_str[value])
        entry.bind("<FocusIn>", self.select_combobox_text)
        return entry

    def create_entry_widgets(self, jdf_nodes):
        """Update the editor section with entry fields."""

        self.parent.destroy_splash()
        self.frame.destroy()

        self.frame = VerticalScrollFrame(self,
                                         (self.screen_height - (
                                             self.screen_height * 0.3)))
        self.frame.grid(row=0, column=0, sticky=tk.NSEW)

        for i, (path, label) in enumerate(jdf_nodes, 1):
            frame = ttk.Frame(self.frame.inner)
            frame.grid(row=i, column=0, padx='20', sticky=tk.NSEW)

            flat_path = flatten_integer_path(path)

            ttk.Label(frame, style="label_style.TLabel",
                      text=('\n' + ARROW_SYM + ' ').join(flat_path)
                      ).grid(row=0, column=0, columnspan=2, sticky=tk.NW)

            column = 0
            row = i

            for key, val in label.items():
                ttk.Label(frame, text=key, justify=tk.LEFT).grid(row=row,
                        column=column, sticky=tk.NW)

                v = val['buffered_value']

                if isinstance(v, list):
                    entry = self._create_list_entry(frame, v)
                elif isinstance(v, bool):
                    entry = self._create_combobox_entry(frame, v)
                else:
                    entry = self._select_entry_by_type(type(v))(frame, value=v)

                entry.config(justify=tk.LEFT)

                entry.grid(row=row + 1, column=column, sticky=tk.NW)
                entry.bind("<FocusOut>", lambda event, flat_keys=(path + (key,)):
                    self.save_entry_value(event, flat_keys))

                column, row = self._accommodate_rows(column, row)

    def select_combobox_text(self, event):
        event.widget.selection_range(0, tk.END)

    def save_entry_value(self, event, flat_keys):
        """ Save entry widget value to internal dictionary. """

        value = event.widget.get()
        self.parent.update_json_dict(value, flat_keys)
        self.parent.refresh_monitor(flat_keys)
