=============================
JSON Knife
=============================

.. image:: https://badge.fury.io/py/json_knife.png
    :target: http://badge.fury.io/py/json_knife

Graphical user interface to manipulate JSON files.

.. image:: ../docs/images/change.png
.. image:: ../docs/images/create_object.png
.. image:: ../docs/images/delete_object.png

Features
--------

* TODO

