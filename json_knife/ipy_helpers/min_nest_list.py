import json
from pathlib import Path
from json_knife import ROOTPATH
p = Path(ROOTPATH, 'test/data/minimal_nest_list.json')
with open(p, 'r') as handle:
    min_nested = json.load(handle)
