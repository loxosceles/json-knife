#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Imports
import tkinter as tk
from tkinter import ttk
from json_knife.dialogs.dialog_window import Dialog
from json_knife.utils.validating_entry import (ArrayEntry,
                                               IntegerEntry,
                                               FloatEntry,
                                               StringEntry)
from json_knife.utils.helpers import flatten_integer_path
from json_knife.constants import ARROW_SYM


class CreateDialog(Dialog):
    def __init__(self, parent, title):
        self.parent = parent
        self.tab_widgets = []
        self.tab_ids = ["Array", "Float", "Integer", "String"]
        self.checked = tk.IntVar()
        self.node_list = []
        Dialog.__init__(self, parent, title)

    def body(self, root):
        self.options_frame = ttk.Frame(root)
        self.options_frame.pack()

        self.n = ttk.Notebook(root)

        # create tabs
        for element in self.tab_ids:
            tf = ttk.Frame(self.n, padding=20)
            self.tab_widgets.append(self.create_tab(tf, element))
            self.n.add(tf, text=element)

        # checkbutton
        self.checked.set(0)
        self.obj_cb = ttk.Checkbutton(self.options_frame,
                                      variable=self.checked,
                                      text="Nest inside new object",
                                      onvalue=1,
                                     offvalue=0,)
        self.obj_cb.grid(row=0, column=0)
        self.obj_cb.configure(command=lambda e=[x[1] for x in self.tab_widgets],
                v=self.checked: self.toggle_objectname_field(e, v))

        self.n.pack()

    def create_tab(self, tab_frame, value_type):
        """Create tab for a specific type.

        :tab:        frame
        :value_type: data type for which this tab serves as input
        """

        nodes = ttk.Combobox(tab_frame)
        nodes.config(width=self._calc_cb_length())
        obj_key_entry = StringEntry(tab_frame, state=tk.DISABLED)
        key_entry = StringEntry(tab_frame)
        value_entry = self._get_type_func(value_type, tab_frame)

        # Node label
        ttk.Label(tab_frame, style="id_label_style.TLabel", text="Node"
                  ).grid(row=0, column=0, sticky=tk.NW)

        # key list config and placement

        self.node_list, node_labels = self.parent.data_object.node_list()
        list_items = [ARROW_SYM.join(i) for i in [self.flatten_integer_path(x)
                                                  for x in self.node_list]]
        nodes['values'] = list_items
        try:
            nodes.current(0)
        except tk.TclError:
            pass

        nodes.grid(row=1, column=0, sticky=tk.NW)

        # Object key label
        ttk.Label(tab_frame, style="id_label_style.TLabel", text="Object Key"
                  ).grid(row=2, column=0, sticky=tk.NW)

        # Object key entry
        obj_key_entry.grid(row=3, column=0, sticky=tk.NW)

        # Key label
        ttk.Label(tab_frame, style="id_label_style.TLabel", text="Key"
                  ).grid(row=4, column=0, sticky=tk.NW)

        # Key entry
        key_entry.grid(row=5, column=0, sticky=tk.NW)

        # Value label
        ttk.Label(tab_frame, style="id_label_style.TLabel", text="Value"
                  ).grid(row=6, column=0, sticky=tk.NW)

        # Value entry
        value_entry.grid(row=7, column=0, sticky=tk.NW)

        return [nodes, obj_key_entry, key_entry, value_entry]

    def _get_type_func(self, typ, tab):
        return {'Array': ArrayEntry, 'Float': FloatEntry, 'Integer': IntegerEntry,
                'String': StringEntry}[typ](tab)

    def _validate_entry_fields(self, values):
        if not all(values):
            return 0
        return 1

    def _backtransform_list(self, string):
        try:
            return [int(x) for x in ' '.join(string.split()).split(' ')]
        except ValueError:
            return []

    def _strip_spaces(self, string):
        return string.strip()

    def _cast_value(self, tab, value):
        return {0: self._backtransform_list, 1: float, 2: int, 3:
                self._strip_spaces}[tab](value)

    def _get_field(self, active_tab, field_name):
        idx = {'node': 0, 'object_key': 1, 'key': 2, 'value': 3}[field_name]
        return self.tab_widgets[active_tab][idx].get()

    def validate(self):
        active_tab = self.n.index(self.n.select())
        values = []
        values.append(self._get_field(active_tab, 'node'))
        values.append(self._get_field(active_tab, 'key'))
        values.append(self._get_field(active_tab, 'value'))

        if self.checked.get() == 1:
            values.append(self._get_field(active_tab, 'object_key'))

        validation = self._validate_entry_fields(values)
        return validation

    def apply(self):
        active_tab = self.n.index(self.n.select())

        node = self._get_field(active_tab, 'node')
        node = tuple(node.strip().split(ARROW_SYM))
        key = self._get_field(active_tab, 'key')
        key = key.strip()
        value = self._get_field(active_tab, 'value')
        value = self._cast_value(active_tab, value)

        if self.checked.get() == 1:
            object_key = self._get_field(active_tab, 'object_key')
            object_key = object_key.strip()
            node = node + (object_key,)

        flat_keys = (node + (key,))

        self.parent.apply_create_dialog(value, flat_keys)

    def toggle_objectname_field(self, entry_widgets, checkbox):
        for ew in entry_widgets:
            if checkbox.get() == 0:
                ew.configure(state=tk.DISABLED)
            else:
                ew.configure(state=tk.NORMAL)
