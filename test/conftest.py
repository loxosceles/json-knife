#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports
import pytest
import json
from decimal import Decimal
from os import path

from .data.data_structures import (tokens, lstripped_tokens,
                                   node_list_long_nested,
                                   node_set_long_nested)

#  Constants
TEST_ROOT = path.dirname(path.abspath(__file__))
TEST_DATA = path.join(TEST_ROOT, 'data')

#  def pytest_configure():
#      pytest.DATA_ROOT = path.join(ROOT_DIR, 'test/data')

@pytest.fixture(scope="session")
def long_nested_tokens():
    return tokens

@pytest.fixture(scope="session")
def long_nested_lstripped_tokens():
    return lstripped_tokens

@pytest.fixture(scope="session")
def file_path_json_min():
    return path.join(TEST_DATA, 'minimal.json')

@pytest.fixture(scope="session")
def file_path_json_min_nest():
    return path.join(TEST_DATA, 'minimal_nest.json')

@pytest.fixture(scope="session")
def flat_dict_min():
    return {
               ('meta',): {
                   'architecture': {
                       'buffered_value': 'cnn',
                       'original_value': 'cnn',
                       'coords': [Decimal('3.24'),
                                  Decimal('3.30')],
                   }
                },
                ('meta', 'layers'):
                    {'conv2d': {
                        'buffered_value': True,
                        'original_value': True,
                        'coords': [Decimal('5.22'),
                                   Decimal('5.26')],
                    }
                },
                ('training_params',): {
                    'epochs': {
                        'buffered_value': 2,
                        'original_value': 2,
                        'coords': [Decimal('9.18'),
                                   Decimal('9.19')],
                    }
                }
            }

@pytest.fixture(scope="session")
def flat_dict_min_nest():
    return {
        (('meta',), {
            'architecture': {
                'buffered_value': 'cnn',
                'original_value': 'cnn',
                'coords': [
                    Decimal('3.24'),
                    Decimal('3.30')
                ]
            }, 'input_seq': {
                'buffered_value': True,
                'original_value': True,
                'coords': [
                    Decimal('22.21'),
                    Decimal('22.25')
                ]
            }
        }),
        (('meta', 'layers', 0, 'conv2d'), {
            'kernel': {
                'buffered_value': [
                    3,
                    3
                ],
                'original_value': [
                    3,
                    3
                ],
                'coords': [
                    Decimal('7.30'),
                    Decimal('10.30')
                ]
            }
        }),
        (('meta', 'layers', 1, 'conv2d'), {
            'kernel': {
                'buffered_value': [
                    2,
                    2
                ],
                'original_value': [
                    2,
                    2
                ],
                'coords': [
                    Decimal('15.30'),
                    Decimal('18.30')
                ]
            }
        }), (('training_params',), {
            'epochs': {
                'buffered_value': 2,
                'original_value': 2,
                'coords': [
                    Decimal('25.18'),
                    Decimal('25.19')
                ]
            }
        })}

@pytest.fixture(scope="session")
def json_dict(file_path_json_min):
    with open(file_path_json_min, 'r') as handle:
        json_dict = json.load(handle)
    return json_dict

@pytest.fixture(scope="session")
def json_dict_nest(file_path_json_min_nest):
    with open(file_path_json_min_nest, 'r') as handle:
        json_dict_nest = json.load(handle)
    return json_dict_nest

@pytest.fixture(scope="session")
def json_dict_min_nest(file_path_json_min_nest):
    with open(file_path_json_min_nest, 'r') as handle:
        json_dict_nest = json.load(handle)
    return json_dict_nest

@pytest.fixture(scope="session")
def json_dict_long_nested():
    with open(path.join(TEST_DATA, 'long_nested.json'), 'r') as handle:
        json_dict_long_nested = json.load(handle)
    return json_dict_long_nested

@pytest.fixture(scope="session")
def path_tup_min():
    return ('training_params', 'epochs')

@pytest.fixture(scope="session")
def path_tup_min_nest():
    return ('meta', 'layers', 1, 'conv2d2', 'kernel')

@pytest.fixture(scope="session")
def node_list():
    return node_list_long_nested

@pytest.fixture(scope="session")
def node_set():
    return node_set_long_nested
