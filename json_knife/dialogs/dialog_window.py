#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Imports
import tkinter as tk
from tkinter import ttk
import os


class Dialog(tk.Toplevel):
    def __init__(self, parent, title=None):
        tk.Toplevel.__init__(self, parent)
        self.transient(parent)
        self.resizable(height=False, width=False)

        self.parent = parent

        if title:
            self.title(title)

        #  self.result = None

        body = ttk.Frame(self)
        self.initial_focus = self.body(body)
        body.pack(padx=5, pady=5)

        self.buttonbox()

        self.grab_set()

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.cancel)

        self.geometry("+%d+%d" % (parent.winfo_rootx() + 50,
                                  parent.winfo_rooty() + 50))

        self.initial_focus.focus_set()

    def body(self, master):
        pass

    def buttonbox(self):
        """Standard button box."""

        box = ttk.Frame(self)
        w = ttk.Button(box, text="OK", width=10, command=self.ok, default=tk.ACTIVE)
        w.pack(side=tk.LEFT, padx=5, pady=5)
        w = ttk.Button(box, text="Cancel", width=10, command=self.cancel)
        w.pack(side=tk.LEFT, padx=5, pady=5)

        self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.cancel)

        box.pack()

    def ok(self, event=None):
        """Ok button."""

        if not self.validate():
            self.initial_focus.focus_set() # put focus back
            return

        self.withdraw()
        self.update_idletasks()

        self.apply()

        self.cancel()

    def cancel(self, event=None):
        """Cancel button."""

        self.parent.focus_set()
        self.destroy()

    def validate(self):
        return 1

    def apply(self):
        pass

    def _calc_cb_length(self):
        upper = 40
        lower = 20
        if self.node_list:
            return max(min(max([len(''.join(str(x))) + 1
                for x in self.node_list]), upper), lower)
        else:
            return lower

    def launch_window(self):
        self.wait_window(self)
