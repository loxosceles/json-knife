#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def flatten_integer_path(tup_path):
    """Convert path with integer indexes into printable path."""

    ll = []
    for el in tup_path:
        if isinstance(el, int):
            ll[-1] = "{} ({})".format(ll[-1], str(el))
        else:
            ll.append(el)
    return tuple(ll)
