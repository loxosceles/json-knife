#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Imports
import pytest
from decimal import Decimal
import json

#  import tkinter as tk
from json_knife.components.data_object import DataObject

@pytest.fixture
def wo_spaces():
    return ['{',
            '"meta": {',
            '"architecture": "cnn",',
            '"layers": {',
            '"conv2d": true',
            '}',
            '},',
            '"training_params": {',
            '"epochs": 2',
            '}',
            '}']

@pytest.fixture
def wo_spaces_nested():
    return ['{',
            '"meta": {',
            '"architecture": "cnn",',
            '"layers": [',
            '{',
            '"conv2d1": {',
            '"kernel": [',
            '3,',
            '3',
            ']',
            '}',
            '},',
            '{',
            '"conv2d2": {',
            '"kernel": [',
            '2,',
            '2',
            ']',
            '}',
            '}',
            '],',
            '"input_seq": true',
            '},',
            '"training_params": {',
            '"epochs": 2',
            '}',
            '}']


class TestDataObject:

    @classmethod
    def setup_class(cls):
        cls.do = DataObject()

    def test_if_data_object_is_initiated(self):
        assert self.do

    def test_fo_json_dict_gets_parsed_correctly(self, json_dict, file_path_json_min):
        self.do.import_file(file_path_json_min)
        assert json_dict == self.do.json_dict

    def test_fo_json_dict_min_nest_gets_parsed_correctly(self, json_dict_nest,
                                                         file_path_json_min_nest):
        self.do.import_file(file_path_json_min_nest)
        assert json_dict_nest == self.do.json_dict

    def test_fo_flat_dict(self, flat_dict_min, file_path_json_min):
        self.do.import_file(file_path_json_min)
        assert flat_dict_min == self.do.flat_dict

    def test_gen_flat_key_dict_builds_flat_dict_correctly(self):
        tuple_path = ('training_params', 'epochs')
        result = self.do.gen_value_coords(tuple_path)
        assert result == (Decimal('9.18'), Decimal('9.19'))

    @pytest.mark.parametrize("row, col, expected", [
        (10, 2, Decimal('10.2')),
        (10, 21, Decimal('10.21')),
        (10, 20, Decimal('10.20')),
        (100, 21, Decimal('100.21')),
        (1000, 999, Decimal('1000.999')),
    ])
    def test_convert_ints_to_coord(self, row, col, expected):
        assert self.do._convert_ints_to_coord(row, col) == expected

    def test_find_line_number(self, wo_spaces):
        path_tuple = ('training_params', 'epochs')
        assert self.do._find_line_number(wo_spaces, path_tuple) == 9

    @pytest.mark.parametrize("tuple_path, expected", [
        (('layers', 0, 'layer'), 9),
        (('layers', 11, 'layer'), 96),
        (('layers', 12, 'config', 'activation'), 102),
        (('layers', 15, 'layer'), 118),
        (('layers', 16, 'config', 'activation'), 124),
        (('audio_params', 'frame_size'), 159),
    ])
    def test_find_line_number_nested(self, tuple_path, expected,
                                     long_nested_lstripped_tokens):
        assert self.do._find_line_number(long_nested_lstripped_tokens,
                                         tuple_path) == expected

    @pytest.mark.parametrize("tuple_path, expected", [
        (('layers', 0, 'layer'), (Decimal('9.21'), Decimal('9.30'))),
        (('layers', 1, 'config', 'pool_size'), (Decimal('19.29'), Decimal('22.29'))),
        (('layers', 10, 'config', 'units'), (Decimal('92.25'), Decimal('92.28'))),
        (('layers', 11, 'layer'), (Decimal('96.21'), Decimal('96.42'))),
        (('layers', 12, 'layer'), (Decimal('100.21'), Decimal('100.34'))),
        (('layers', 12, 'config', 'activation'), (Decimal('102.30'), Decimal('102.36'))),
        (('layers', 15, 'layer'), (Decimal('118.21'), Decimal('118.42'))),
        (('layers', 16, 'config', 'activation'), (Decimal('124.30'), Decimal('124.36'))),
        (('training_params', 'batch_size'), (Decimal('143.22'), Decimal('143.26'))),
    ])
    def test_gne_value_coords(self, tuple_path, expected, json_dict_long_nested):
        self.do.json_dict = json_dict_long_nested
        assert self.do.gen_value_coords(tuple_path) == expected

    def test_delete_arr_index_deletes_correct_item(self):
        tups = ('a', 'b', 0, 'c', 0, 'd', 1)
        idx = 0
        result = self.do._delete_arr_index(tups, idx)
        assert result == ('a', 'b', 'c', 0, 'd', 1)

    def test_filter_sublist(self, wo_spaces_nested):
        expected = ['{',
                    '"conv2d1": {',
                    '"kernel": [',
                    '3,',
                    '3',
                    ']',
                    '}',
                    '},',
                    '{',
                    '"conv2d2": {',
                    '"kernel": [',
                    '2,',
                    '2',
                    ']',
                    '}',
                    '}',
                    '],',
                    '"input_seq": true',
                    '},',
                    '"training_params": {',
                    '"epochs": 2',
                    '}',
                    '}']
        sublist = self.do._filter_sublist(wo_spaces_nested[4:], 0)[0]
        assert sublist == expected

    def test_filter_sublist_nested(self, wo_spaces_nested):
        expected = ['{',
                    '"conv2d2": {',
                    '"kernel": [',
                    '2,',
                    '2',
                    ']',
                    '}',
                    '}',
                    '],',
                    '"input_seq": true',
                    '},',
                    '"training_params": {',
                    '"epochs": 2',
                    '}',
                    '}']
        sublist = self.do._filter_sublist(wo_spaces_nested[4:], 1)[0]
        assert sublist == expected

    def test_count_array_length(self):
        arr = '3,', '3', ']'
        assert self.do._count_array_length(arr) == 3

    def test_dyn_dict_set(self, json_dict_min_nest):
        self.do.json_dict = json_dict_min_nest
        value = 'new value'
        flat_keys = ('meta', 'layers', 0, 'conv2d1', 'kernel')
        assert self.do.json_dict['meta']['layers'][0]['conv2d1']['kernel'] == [3, 3]
        self.do.dyn_dict_set(value, flat_keys)
        assert self.do.json_dict['meta']['layers'][0]['conv2d1']['kernel'] == 'new value'

    def test_token_list_is_created_correctly(self, json_dict_long_nested,
                                             long_nested_tokens):
        t, _ = self.do._split_dict_string(json.dumps(json_dict_long_nested,
                                                     indent=4))
        assert t == long_nested_tokens

    def test_lstripped_token_list_is_created_correctly(self, json_dict_long_nested,
                                                       long_nested_lstripped_tokens):
        _, lst = self.do._split_dict_string(json.dumps(json_dict_long_nested,
                                                       indent=4))
        assert lst == long_nested_lstripped_tokens

    @pytest.mark.parametrize("original, v_shift, h_shift, expected", [
        (Decimal('10.5'), -3, 0, Decimal('7.5')),
        (Decimal('10.5'), -3, -4, Decimal('7.1')),
        (Decimal('100.5'), 3, 0, Decimal('103.5')),
        (Decimal('10.5'), 3, 6, Decimal('13.11')),
        (Decimal('143.22'), 3, 6, Decimal('146.28')),
        (Decimal('143.22'), Decimal('0'), Decimal('0'), Decimal('143.22')),
    ])
    def test_vh_update(self, original, v_shift, h_shift, expected):
        value = original
        updated = self.do.vh_update(value, v_shift, h_shift)
        assert updated == expected

    @pytest.mark.parametrize("value, v_shift, expected", [
        (Decimal('143.22'), Decimal('0'), Decimal('143.22')),
        (Decimal('143.22'), Decimal('1'), Decimal('144.22')),
    ])
    def test_add_vertical_shift(self, value, v_shift, expected):
        assert self.do._add_vertical_shift(value, v_shift) == expected

    @pytest.mark.parametrize("value, v_shift, h_shift, expected", [
        (Decimal('143.22'), Decimal('0'), Decimal('0'), Decimal('143.22')),
        (Decimal('143.22'), Decimal('1'), Decimal('1'), Decimal('144.23')),
    ])
    def test_add_vertical_horizontal_shift(self, value, v_shift, h_shift, expected):
        assert self.do._add_vertical_horizontal_shift(value, v_shift, h_shift) == expected

    @pytest.mark.skip
    @pytest.mark.parametrize("v_shift, h_shift, expected", [
        (3, -6, Decimal('13.11'))
    ])
    def test_vh_update_throws_exc_with_negative_values(self, v_shift, h_shift, expected):
        value = Decimal('10.5')
        updated = self.do.vh_update(value, v_shift, h_shift)
        assert updated == expected

    @pytest.mark.parametrize("val_type, ref_type, expected", [
        ("1", 1, int),
        ("1.1", 1.1, float),
        ("string", "string", str),
        ("True", True, bool),
        ("1 2", [1, 2], list)
    ])
    def test_cast_string_value(self, val_type, ref_type, expected):
        result = self.do._cast_string_value(ref_type, val_type)
        assert type(result) == expected

    def test_node_list(self, json_dict_long_nested, node_list):
        self.do.json_dict = json_dict_long_nested
        assert self.do.node_list()[1] == node_list

    def test_node_set(self, json_dict_long_nested, node_set):
        self.do.json_dict = json_dict_long_nested
        assert self.do.node_list()[0] == node_set

    @classmethod
    def teardown_class(cls):
        pass
