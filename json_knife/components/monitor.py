#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Imports
import tkinter as tk
from tkinter import ttk
from json_knife.splash_screen import SPLASHSCREEN_TEXT


class Monitor(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        # style variables
        self.splashscreen_width = 80
        self.textfield_width = 50
        self.textfield_font = ('Consolas', 10)

        self.startup_screen = tk.StringVar()

        self.frame_style = ttk.Style()
        self.frame_style.configure("TFrame", pady=20)
        self.configure(style="TFrame")

        self.startup_screen.set(SPLASHSCREEN_TEXT)
        self.create_textfield()
        self.create_splash()
        self.textfield.tag_config('match', foreground='red')

    def create_textfield(self):
        self.scrollbar = ttk.Scrollbar(self)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.textfield = tk.Text(self, font=self.textfield_font,
                yscrollcommand=self.scrollbar.set,
                width=self.splashscreen_width)
        # make textfield copy-pastable
        self.textfield.bind("<1>", lambda event: self.textfield.focus_set())
        self.textfield.pack(fill=tk.BOTH, expand=1)
        self.scrollbar.config(command=self.textfield.yview)

    def create_splash(self):
        self.splash_style = ttk.Style()
        self.splash = ttk.Label(self.textfield, textvariable=self.startup_screen,
                style="SPLASH.TLabel")
        self.splash_style.configure("SPLASH.TLabel", font=('Courier', 8, 'bold'),
                background="white", anchor=tk.CENTER, padding=50)
        self.splash.pack()

    def destroy_splash(self):
        self.splash.destroy()

    def insert(self, string):
        self.textfield.configure(state=tk.NORMAL)
        self.textfield.delete(1.0, tk.END)
        self.textfield.insert(1.0, string)
        self.textfield.configure(state=tk.DISABLED)

    def refresh(self, string, dtags, end=None):
        self.textfield.configure(state=tk.NORMAL)
        self.textfield.delete(1.0, tk.END)
        self.textfield.insert(1.0, string)

        for coords in dtags:
            self.textfield.tag_add('match', coords[0], coords[1])

        self.textfield.configure(state=tk.DISABLED)

        if end:
            self.parent.monitor.textfield.see(end)

    def adjust_textfield(self):
        self.textfield.configure(width=self.textfield_width)
