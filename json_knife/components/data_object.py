#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Imports
from collections import OrderedDict
from orderedset import OrderedSet
from decimal import Decimal, getcontext
from functools import reduce
import json
import operator
from copy import deepcopy


class DataObject:
    """Object holding configuration variables. """

    def __init__(self):
        self.flat_dict = OrderedDict()
        self.flat_dict_static = OrderedDict()
        self.json_dict = OrderedDict()
        self.dirty_tags = set()
        self._name = ""

        self._textfield_length = 0
        self.previous_value = ""

    @property
    def json_str(self):
        """JSON string representation."""

        return json.dumps(self.json_dict, indent=4)

    @property
    def name(self):
        return self._name

    @property
    def prev_tf_length(self):
        return self._textfield_length

    @property
    def curr_tf_length(self):
        return len(self.json_str.split('\n'))

    @name.setter
    def name(self, name):
        self._name = name

    def is_live(self):
        return True if self.name else False

    def is_empty(self):
        return True if not self.json_dict else False

    def import_file(self, json_file):
        """Import configurations from tf_conf.json.

        :json_file: Name of the json file on disk
        :returns:   None
        """

        try:
            with open(json_file, 'r') as infile:
                self.json_dict = json.load(infile)
                self.gen_file_dict(self.json_dict)
                self.gen_flat_key_dict(self.json_dict)
        except ValueError as e:
            print(e)
            print("Decoding the JSON config file has failed. Please make sure the\
                    format is correct.")

    def _cast_string_value(self, ref_value, string_value):
        return {list: lambda val: [int(x) for x in ' '.join(val.split()).split(' ')],
                int: lambda x: int(x),
                float: lambda x: float(x),
                bool: lambda x: {"true": True, "false": False}[x.lower()],
                str: lambda x: x}[type(ref_value)](string_value)

    def update_json_dict(self, value, flat_keys):
        path = flat_keys[:-1]
        label = flat_keys[-1]
        fd_value = self.flat_dict[path][label]['buffered_value']

        #  cast the string value into original type
        value = self._cast_string_value(fd_value, value)

        # set textfield field length before it changes (on save)
        self.set_prev_tf_length()

        # save previous value for column comparison
        # FIXME: move this into flat_dict
        self.previous_value = self.dyn_dict_get(flat_keys)

        # save value to master dict
        self.dyn_dict_set(value, flat_keys)

        # call the update method of the monitor
        self.update_tags(value, flat_keys)

    def gen_file_dict(self, json_dict):
        """ Parse json object recursivly and build a dictionary holding (flat)
        path and value.

        This method serves as a representation of the read-in file. The resulting
        dictionary will not be modified during execution. Instead, it holds the
        original structure of the file.

        :jsd:    The json dictionary which was built from the file
        :jsfrep: The resulting dictionary as a representation of the file
        """

        def _gen_dict(d, path_tuple=tuple()):
            if not isinstance(d, dict):
                label = path_tuple[-1]
                path = path_tuple[:-1]

                if not self.flat_dict_static.get(path):
                    self.flat_dict_static[path] = {}

                self.flat_dict_static[path][label] = {}
                self.flat_dict_static[path][label]['original_value'] = d

            else:
                for key, value in d.items():
                    if isinstance(value, list) and not isinstance(value[0], dict):
                        _gen_dict(value, path_tuple + (key,))
                    elif isinstance(value, list) and isinstance(value[0], dict):
                        for i, el in enumerate(value):
                            _gen_dict(el, path_tuple + (key, i))
                    else:
                        _gen_dict(value, path_tuple + (key,))

        self.flat_dict_static.clear()
        _gen_dict(json_dict)

    def gen_flat_key_dict(self, json_dict):
        """ Parses json object recursively and returns path and value.

        :returns: None
        """

        def _gen_dict(d, path_tuple=tuple()):
            if not isinstance(d, dict):
                label = path_tuple[-1]
                path = path_tuple[:-1]

                if not self.flat_dict.get(path):
                    self.flat_dict[path] = {}

                self.flat_dict[path][label] = {}
                self.flat_dict[path][label]['buffered_value'] = d

                try:
                    self.flat_dict[path][label]['original_value'] =\
                        self.flat_dict_static[path][label]['original_value']
                except KeyError:
                    self.flat_dict[path][label]['original_value'] = None

                start, end = self.gen_value_coords(path_tuple)
                self.flat_dict[path][label]['coords'] = [start, end]
            else:
                #  for key, value in d.items():
                #      gen_dict(value, path_tuple + (key,))
                for key, value in d.items():
                    if isinstance(value, list) and not isinstance(value[0], dict):
                        _gen_dict(value, path_tuple + (key,))
                    elif isinstance(value, list) and isinstance(value[0], dict):
                        for i, el in enumerate(value):
                            _gen_dict(el, path_tuple + (key, i))
                    else:
                        _gen_dict(value, path_tuple + (key,))

        self.flat_dict.clear()
        _gen_dict(json_dict)

    def _split_dict_string(self, ds):
        l = ds.split('\n')
        return l, [x.lstrip() for x in l]

    def _count_array_length(self, l):
        count = 0
        for el in l:
            count += 1
            if el.endswith('],') or el.endswith(']'):
                return count

    def _filter_sublist(self, l, cycle, br_count=0, lines=0):
        if cycle == 0 and br_count == 0:
            return l, lines# + 1
        else:
            if '{' in l[0]:
                br_count += 1
            if '}' in l[0]:
                br_count -= 1
            if br_count == 0:
                cycle -= 1
            l = l[1:]
            lines += 1
            return self._filter_sublist(l, cycle, br_count, lines)

    def _delete_arr_index(self, tups, arr_idx):
        """Delete the list index from the path tuple."""

        return tups[:tups.index(arr_idx)] + tups[tups.index(arr_idx) + 1:]

    def _detect_level(self, element, level):
        """Update the level count."""
        if "{" in element:
            level += 1
        if "}" in element:
            level -= 1
        return level

    def _find_line_number(self, slices, tup_path, lev=0, ind=0, lines=0):
        """Find the starting line number in the string representation."""

        slices = slices[1:]

        if ind == len(tup_path):
            return 1 + lines
        else:
            el = slices[0]
            try:
                if el.startswith('"' + tup_path[ind]) and\
                        tup_path.index(tup_path[ind]) == lev:
                    ind += 1
            except TypeError:
                #  integer indicating an list index
                slices, lines = self._filter_sublist(slices, tup_path[ind])
                #  delete the index from the path tuple
                tup_path = self._delete_arr_index(tup_path, tup_path[ind])
            except IndexError:
                #  no keyword found, skip
                pass

            lev = self._detect_level(el, lev)
            return 1 + self._find_line_number(slices, tup_path, lev, ind, lines)

    def _convert_ints_to_coord(self, row, col):
        """Convert an integer into decimal notation."""

        return Decimal(str(row) + '.' + str(col))

    def gen_value_coords(self, path):
        """Find the value's coordinates in the string representation."""

        tokens, lstripped_tokens = self._split_dict_string(self.json_str)
        start_row = self._find_line_number(lstripped_tokens, path)
        idx = start_row - 1
        start_col = tokens[idx].find(':') + 2

        if tokens[idx].endswith('[') and not tokens[idx + 1] == '{':
            end_row = self._count_array_length(tokens[idx + 1:]) + start_row
            end_col = start_col
        else:
            end_row = start_row
            end_col = len(tokens[idx])

        start = self._convert_ints_to_coord(start_row, start_col)
        end = self._convert_ints_to_coord(end_row, end_col)

        return start, end

    def dyn_dict_get(self, flat_keys):
        try:
            return reduce(operator.getitem, flat_keys, self.json_dict)
        except KeyError:
            pass

    def dyn_dict_set(self, value, flat_keys):
        """Set a value in a nested dictionary."""

        dic = self.json_dict
        for key in flat_keys[:-1]:
            if isinstance(key, int):
                dic = dic[key]
            else:
                dic = dic.setdefault(key, {})
        dic[flat_keys[-1]] = value

    def dyn_dict_delete(self, flat_keys):
        del self.dyn_dict_get(flat_keys[:-1])[flat_keys[-1]]

    def get_coords(self, flat_keys, position):
        """TODO: Docstring for function.

        :arg1: TODO
        :returns: TODO
        """

        path = flat_keys[:-1]
        label = flat_keys[-1]
        return self.flat_dict[path][label]['coords'][position]

    def node_list(self):
        node_set = OrderedSet()
        node_key_dict = OrderedDict()

        def get_list(d, path_tuple=tuple()):
            if not isinstance(d, dict):
                label = path_tuple[-1]
                path = path_tuple[:-1]
                if node_key_dict.get(path):
                    node_key_dict[path].append(label)
                else:
                    node_key_dict[path] = [label]
                return
            else:
                for key, value in d.items():
                    if isinstance(value, dict):
                        node_set.add(path_tuple + (key,))
                    elif isinstance(value, list) and isinstance(value[0], dict):
                        for i, el in enumerate(value):
                            get_list(el, path_tuple + (key, i))
                    else:
                        node_set.add(path_tuple)

                    get_list(value, path_tuple + (key,))

        get_list(self.json_dict)
        node_set.discard('')
        print(list(node_set))
        print(node_key_dict)
        return list(node_set), node_key_dict

    def is_field_dirty(self, flat_keys, value):
        """Boolean method which checks if a field has been modified.

        :keys:    flat keys in order to find the value
        :value:   current value
        :returns: True if the value differs from the original one, else False
        """

        path = flat_keys[:-1]
        label = flat_keys[-1]
        if self.flat_dict[path][label]['original_value'] != value:
            return True
        else:
            return False

    def set_prev_tf_length(self):
        """Determine the total line number written to the textfield."""

        self._textfield_length = len(self.json_str.split('\n'))

    def clean_dirty_tags(self, flat_keys):
        for el in deepcopy(self.dirty_tags):
            if flat_keys == el[:len(flat_keys)]:
                self.dirty_tags.discard(el)

    def _add_vertical_shift(self, value, v_shift):
        return value + v_shift

    def _add_vertical_horizontal_shift(self, value, v_shift, h_shift):
        aux = list(map(lambda x: Decimal(x), str(value).split('.')))
        aux[0] = str(aux[0] + v_shift)
        aux[1] = str(aux[1] + h_shift)
        value = Decimal('.'.join(aux))
        return value

    def vh_update(self, value, v_shift, h_shift=None):
        if h_shift is None:
            value = self._add_vertical_shift(value, v_shift)
        else:
            value = self._add_vertical_horizontal_shift(value, v_shift, h_shift)
        return value

    def shift_positions(self, start_idx, v_shift, h_shift):
        for obj in self.flat_dict.values():
            for label in obj.values():
                start_cds = label['coords'][0]
                end_cds = label['coords'][1]
                if end_cds == start_idx:
                    label['coords'][1] = self.vh_update(end_cds, v_shift, h_shift)
                elif start_cds > start_idx:
                    label['coords'][0] = self.vh_update(start_cds, v_shift)
                    label['coords'][1] = self.vh_update(end_cds, v_shift)

    def _compute_line_diff(self):
        return self.curr_tf_length - self.prev_tf_length

    def _compute_column_diff(self, value):
        return len(str(value)) - len(str(self.previous_value))

    def update_tags(self, value, flat_keys):
        end = self.get_coords(flat_keys, 1)
        line_diff = self._compute_line_diff()
        column_diff = self._compute_column_diff(value)
        self.shift_positions(end, line_diff, column_diff)
        self.dirty_tags.discard(flat_keys)

        if self.is_field_dirty(flat_keys, value):
            self.dirty_tags.add(flat_keys)
