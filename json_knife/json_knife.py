#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Imports
import tkinter as tk
from tkinter import ttk
from os import path
import json
from json_knife.constants import PROGRAM_NAME
from json_knife.components.data_object import DataObject
from json_knife.components.menu_bar import MenuBar
from json_knife.components.editor import Editor
from json_knife.components.monitor import Monitor
from json_knife.dialogs.create_dialog import CreateDialog
from json_knife.dialogs.delete_dialog import DeleteDialog


class MainApplication(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.screen_width = parent.winfo_screenwidth()
        self.screen_height = parent.winfo_screenheight()
        self.init_data_object()

        # Create GUI elements
        self.monitor = Monitor(self)
        self.editor = Editor(self, screen_height=self.screen_height)

        # Place GUI elements
        self.editor.grid(row=0, column=1)
        self.monitor.grid(row=0, column=0, sticky=tk.NS)

        # Add menu bar
        self.menubar = MenuBar(self)

        menu_bindings = {
            '<Control-O>': self.menubar.open,
            '<Control-o>': self.menubar.open,
            '<Control-S>': self.menubar.save,
            '<Control-s>': self.menubar.save,
            '<Control-Shift-S>': self.menubar.save_as,
            '<Control-Shift-s>': self.menubar.save_as,
            '<Control-Q>': self.quit_monitor,
            '<Control-q>': self.quit_monitor,
            '<Control-Shift-O>': self.menubar.add_object,
            '<Control-Shift-o>': self.menubar.add_object,
            '<Control-Shift-D>': self.menubar.delete_object,
            '<Control-Shift-d>': self.menubar.delete_object,
        }

        for key, val in menu_bindings.items():
            self.parent.bind(key, val)

    def init_data_object(self, file_name=None):
        self.data_object = DataObject()

        if file_name:
            self.data_object.name = file_name
            self.set_title(file_name)
            self.data_object.import_file(file_name)
            self.monitor.insert(self.data_object.json_str)
            self.create_entry_widgets()

    def set_title(self, file_name):
        self.parent.title('{} - {}'.format(path.basename(
                          file_name), self.parent.title()))

    def set_do_name(self, file_name):
        self.data_object.name = file_name

    def update_json_dict(self, value, flat_keys):
        self.data_object.update_json_dict(value, flat_keys)

    def clear_do_tags(self):
        self.data_object.dirty_tags.clear()

    def refresh_monitor(self, flat_keys=None):
        dtags = []

        for fkey in self.data_object.dirty_tags:
            dtags.append([self.data_object.get_coords(fkey, 0),
                          self.data_object.get_coords(fkey, 1)])

        try:
            end = self.data_object.get_coords(flat_keys, 1)
            self.monitor.refresh(self.data_object.json_str, dtags, end)
        except TypeError:
            self.monitor.refresh(self.data_object.json_str, dtags)

    def quit_monitor(self, event=None):
        self.parent.destroy()

    def create_dialog(self):
        self.create_dlg = CreateDialog(self, "Create Object")
        self.create_dlg.launch_window()
        self.parent.wait_window(self.create_dlg.parent.parent)

    def delete_dialog(self):
        self.delete_dlg = DeleteDialog(self, "Delete Object")
        self.create_dlg.launch_window()
        self.parent.wait_window(self.delete_dlg.parent.parent)

    def write_to_file(self):
        """Write current json_dict to file

        :returns: None
        """

        try:
            content = self.data_object.json_dict
            file_name = self.data_object.name

            with open(file_name, 'w') as outfile:
                json.dump(content, outfile)
        except IOError:
            pass

    def create_entry_widgets(self):
        jdf_nodes = self.data_object.flat_dict.items()
        self.editor.create_entry_widgets(jdf_nodes)

    def destroy_splash(self):
        self.monitor.destroy_splash()
        self.monitor.adjust_textfield()

    def apply_create_dialog(self, value, flat_keys):
        print(f'flat_keys (apply_create_dialog): {flat_keys}')
        self.data_object.dyn_dict_set(value, flat_keys)
        self.data_object.gen_flat_key_dict(self.data_object.json_dict)
        self.create_entry_widgets()
        self.data_object.set_prev_tf_length()
        self.data_object.update_tags(value, flat_keys)
        self.refresh_monitor(flat_keys)


if __name__ == "__main__":
    root = tk.Tk()
    root.title(PROGRAM_NAME)
    root.configure(padx=10, pady=10)
    root.columnconfigure(0, minsize=150)
    root.resizable(False, False)
    MainApplication(root).pack(side="top", fill="both", expand=True)
    root.mainloop()
