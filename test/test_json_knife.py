#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Imports
import pytest

import tkinter as tk
from json_knife.json_knife import MainApplication as ma


class TestJsonKnife:

    @classmethod
    def setup_class(cls):
        root = tk.Tk()
        root.title("TestRoot")
        cls.mainapp = ma(root)

    def test_root_has_title(self):
        assert self.mainapp.parent.title() == "TestRoot"

    def test_root_set_title(self):
        ma.set_title(self.mainapp, "test.json")
        assert self.mainapp.parent.title() == "test.json - TestRoot"

    @classmethod
    def teardown_class(cls):
        pass
