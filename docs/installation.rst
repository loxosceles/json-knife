============
Installation
============

At the command line either via easy_install or pip::

    $ easy_install json_knife
    $ pip install json_knife

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv json_knife
    $ pip install json_knife
