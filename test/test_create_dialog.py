#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Imports
import pytest
import tkinter as tk
from tkinter import ttk
from json_knife.dialogs.create_dialog import CreateDialog
from json_knife.utils.validating_entry import (ArrayEntry)
from json_knife.components.data_object import DataObject


class TestCreateDialog:

    @classmethod
    def setup_class(cls):
        root = tk.Tk()
        root.data_object = DataObject()
        cls.notebook = ttk.Notebook(root)
        cls.cd = CreateDialog(root, 'Test')

    @pytest.mark.parametrize("values, expected", [
        (['', '', 'not empty'], 0),
        (['', '', 'not empty', ''], 0),
        (['', '', ''], 0),
        (['', '', '', ''], 0),
        (['one', 'two', 'three', 'four'], 1)
    ])
    def test_validate_entry_fields(self, values, expected):
        assert self.cd._validate_entry_fields(values) == expected

    def test_backtransform_list(self):
        string = "1 2 3 4"
        assert self.cd._backtransform_list(string) == [1, 2, 3, 4]

    def test_backtransform_list_with_empty_string(self):
        string = ""
        assert self.cd._backtransform_list(string) == []

    def test_cast_value(self):
        assert self.cd._cast_value(0, '1 2') == [1, 2]

    def test_get_type_func(self):
        tab = ttk.Frame(self.notebook, padding=20)
        assert isinstance(self.cd._get_type_func('Array', tab),
                          type(ArrayEntry(tab)))

    @classmethod
    def teardown_class(cls):
        pass
