#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Imports
import pytest

import tkinter as tk
from json_knife.components.editor import Editor
from json_knife.utils.validating_entry import (ArrayEntry,
                                               IntegerEntry,
                                               FloatEntry,
                                               StringEntry)

from json_knife.utils.helpers import flatten_integer_path


class TestEditor:

    @classmethod
    def setup_class(cls):
        root = tk.Tk()
        cls.editor = Editor(root, screen_height=10)

    @pytest.mark.parametrize("col, row, expected", [
        (0, 2, (1, 2)),
        (1, 2, (0, 4)),
    ])
    def test_accommodate_rows(self, col, row, expected):
        result = self.editor._accommodate_rows(col, row)
        assert result == expected

    @pytest.mark.parametrize("val_type, expected", [
        (int, IntegerEntry),
        (float, FloatEntry),
        (str, StringEntry)
    ])
    def test_select_entry_by_type(self, val_type, expected):
        func = self.editor._select_entry_by_type(val_type)
        assert func == expected

    def test_flatten_integer_path(self):
        tup_path = ('one', 'two', 0, 'three', 0, 'four')
        result = flatten_integer_path(tup_path)
        assert result == ('one', 'two (0)', 'three (0)', 'four')

    @classmethod
    def teardown_class(cls):
        pass
