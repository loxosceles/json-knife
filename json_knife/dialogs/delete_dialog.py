#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Imports
from json_knife.dialogs.dialog_window import Dialog
import tkinter as tk
from tkinter import ttk
from json_knife.constants import ARROW_SYM


class DeleteDialog(Dialog):
    def __init__(self, parent, title):
        self.parent = parent
        self.node_list, self.node_labels = self.parent.data_object.node_list()
        Dialog.__init__(self, parent, title)

    def body(self, root):
        self.frame = ttk.Frame(root)
        self.frame.pack()
        self.nodes = ttk.Combobox(self.frame, state='readonly')
        self.nodes.config(width=self._calc_cb_length())
        self.key = ttk.Combobox(self.frame, state='readonly')

        # Nodes label
        ttk.Label(self.frame,
                  style="id_label_style.TLabel", text="Node").grid(row=0, column=0,
                                                                   sticky=tk.NW)

        list_items = [ARROW_SYM.join(i) for i in self.node_list]
        self.nodes['values'] = list_items
        self.nodes.bind("<<ComboboxSelected>>", self._update_label_cb)
        self.nodes.bind("<FocusIn>", self._update_label_cb)
        self.nodes.focus_set()
        self.nodes.current(0)
        self.nodes.grid(row=1, column=0, sticky=tk.NW)

        # Labels label
        ttk.Label(self.frame, style="id_label_style.TLabel",
                  text="Key").grid(row=2, column=0, sticky=tk.NW)
        self.key.grid(row=3, column=0, sticky=tk.NW)

    def apply(self):
        node = tuple(self.nodes.get().strip().split(ARROW_SYM))
        key = self.key.get().strip()

        if key:
            node = node + (key,)

        flat_keys = node
        self.parent.data_object.set_prev_tf_length()
        self.parent.data_object.dyn_dict_delete(node)
        self.parent.data_object.gen_flat_key_dict()
        self.parent.create_entry_widgets()
        self.parent.data_object.clean_dirty_tags(flat_keys)
        self.parent.refresh_monitor()
