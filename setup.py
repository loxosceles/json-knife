#!/usr/bin/env python

import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
doclink = """
Documentation
-------------

The full documentation is at http://json_knife.rtfd.org."""
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='json_knife',
    version='0.1.0',
    description='Graphical user interface to manipulate JSON files.',
    long_description=readme + '\n\n' + doclink + '\n\n' + history,
    author='Magnus "Loxosceles" Henkel',
    author_email='loxosceles@gmx.de',
    url='https://gitlab.com/loxosceles/json_knife',
    packages=[
        'json_knife',
        'json_knife/utils',
        'json_knife/dialogs',
        'json_knife/components'
    ],
    package_dir={'json_knife': 'json_knife'},
    include_package_data=True,
    install_requires=[
    ],
    license='GNU GPL v3.0',
    zip_safe=False,
    keywords='json_knife',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU GPL v3.0',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
)
